﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstUML
{
    class Employee
    {
        public Manager _manager;
       
        public void AddManager(Manager pManager)
        {
            _manager = pManager;
        }
        public void message()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("I am Employee");
            Console.ResetColor();
        }
    }
    class Manager:Employee
    {
        public Project MyProject = new Project();
        public Card MyCard { get; set; }
        public void menssageChild()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("I am Manager");
            Console.ResetColor();
        }
    }
    class Card
    {
        public void menssageCard()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("I am Card");
            Console.ResetColor();
        }
    }
    class Project
    {
        public void menssageProject()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("I am Project");
            Console.ResetColor();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            Console.WriteLine("== Class Employee");
            manager.message();
            Console.WriteLine("== Class Manager");
            manager.menssageChild();
            Console.WriteLine("== Class Project");
            manager.MyProject.menssageProject();
            Console.WriteLine("== Class Card");
            Card card = new Card();
            manager.MyCard = card;
            manager.MyCard.menssageCard();

            Console.ReadKey();
        }
    }
}
